#!/usr/bin/env bash

# NOTE:
# - awscli must be installed on server - (sudo) pip install awscli
# - must run `aws configure`

UPLOADS_DIR="$HOME/supost/shared/uploads/"
BUCKET="supost-backup/photos/"

echo "================================="
echo "== SYNC PHOTOS      $(/bin/date +%Y-%m-%d) =="
echo "================================="

echo "** Started at: $(/bin/date)"

echo "-> /usr/local/bin/aws s3 sync --quiet --only-show-errors --exclude '*raw/*' $UPLOADS_DIR s3://$BUCKET"
/usr/local/bin/aws s3 sync --quiet --only-show-errors --exclude "*raw/*" $UPLOADS_DIR s3://$BUCKET

echo "** Finished at: $(/bin/date)"


echo "** Sync photo locally"
echo "** Started at: $(/bin/date)"

UPLOADS_DIR="$HOME/supost/shared/uploads/"
SERVER="root@159.203.192.10"

echo "-> /usr/bin/rsync -az --exclude 'raw/*' $UPLOADS_DIR $SERVER:~/photos/"
/usr/bin/rsync -az --exclude "raw/*" $UPLOADS_DIR "$SERVER:~/photos/"

echo "** Finished at: $(/bin/date)"
