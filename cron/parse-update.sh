#!/bin/sh

set -e

APP_USER="deploy"
APP_ROOT=/home/$APP_USER/supost/current
RBENV_ROOT=/home/$APP_USER/.rbenv
RBENV_VERSION="2.0.0-p648"
PATH=$RBENV_ROOT/shims:$RBENV_ROOT/bin:$PATH

cd $APP_ROOT && bundle exec rake parse:update RAILS_ENV=production
