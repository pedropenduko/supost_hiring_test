#!/usr/bin/env bash

# NOTE:
# - awscli must be installed on server - (sudo) pip install awscli
# - must run `aws configure`

BUCKET="supost-backup/databases"

echo "================================="
echo "== BACKUP DATABASES $(/bin/date +%Y-%m-%d) =="
echo "================================="

echo "** Started at: $(/bin/date)"

YEAR=`/bin/date +%Y`
MONTH=`/bin/date +%m`
DATE=`/bin/date +%Y%m%d`
FILENAME=supost_prod-$DATE.sql.gz

mkdir -p ~/mysql
echo "-> cd ~/mysql"
cd ~/mysql

echo "-> /usr/bin/find ./*.sql.gz -mtime +0 -exec rm {} \\;"
/usr/bin/find ./*.sql.gz -mtime +0 -exec rm {} \;

echo "-> Dumping database to sql file"
/usr/bin/mysqldump --opt --user=root --password=You#NeedToChangeThisPassword! supost_prod | /bin/gzip > $FILENAME

# Extra local backup
SERVER="root@159.203.192.10"
echo "-> /usr/bin/scp $FILENAME $SERVER:~/databases/"
/usr/bin/scp "$FILENAME" "$SERVER:~/databases/"

echo "-> /usr/local/bin/aws s3 cp $FILENAME s3://$BUCKET/$YEAR/$MONTH/$FILENAME"
/usr/local/bin/aws s3 cp $FILENAME s3://$BUCKET/$YEAR/$MONTH/$FILENAME

echo "** Finished at: $(/bin/date)"
