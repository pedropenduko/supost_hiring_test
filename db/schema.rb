# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20161220175005) do

  create_table "categories", :force => true do |t|
    t.string "name"
    t.string "short_name"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",                         :default => 0
    t.integer  "attempts",                         :default => 0
    t.text     "handler",    :limit => 2147483647
    t.text     "last_error", :limit => 2147483647
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "events", :force => true do |t|
    t.string   "stripe_id"
    t.string   "stripe_object"
    t.string   "stripe_type"
    t.string   "api_version"
    t.string   "created"
    t.boolean  "live_mode"
    t.integer  "pending_webbooks"
    t.string   "request"
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "events", ["stripe_id", "stripe_object", "stripe_type"], :name => "index_events_on_stripe_id_and_stripe_object_and_stripe_type"
  add_index "events", ["stripe_id"], :name => "index_events_on_stripe_id"
  add_index "events", ["stripe_object"], :name => "index_events_on_stripe_object"
  add_index "events", ["stripe_type"], :name => "index_events_on_stripe_type"

  create_table "messages", :force => true do |t|
    t.text     "message"
    t.integer  "post_id"
    t.string   "ip"
    t.string   "email"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "payments", :force => true do |t|
    t.integer  "post_id"
    t.string   "seller"
    t.string   "buyer"
    t.decimal  "price",              :precision => 10, :scale => 2
    t.integer  "checkout_type"
    t.decimal  "amount",             :precision => 10, :scale => 2
    t.float    "rate"
    t.date     "date"
    t.string   "token"
    t.string   "stripe_charge"
    t.string   "stripe_source"
    t.string   "stripe_transaction"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "payments", ["checkout_type"], :name => "index_payments_on_checkout_type"
  add_index "payments", ["post_id"], :name => "index_payments_on_post_id"

  create_table "posts", :force => true do |t|
    t.integer  "college_id"
    t.integer  "category_id"
    t.integer  "subcategory_id"
    t.string   "email"
    t.string   "ip"
    t.string   "name"
    t.text     "body"
    t.integer  "security_id"
    t.integer  "time_posted"
    t.integer  "time_modified"
    t.string   "image_source1"
    t.string   "image_source2"
    t.string   "image_source3"
    t.string   "image_source4"
    t.integer  "status"
    t.string   "photo1_file_name"
    t.string   "photo1_content_type"
    t.string   "photo2_file_name"
    t.string   "photo2_content_type"
    t.string   "photo3_file_name"
    t.string   "photo3_content_type"
    t.string   "photo4_file_name"
    t.string   "photo4_content_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_token"
    t.boolean  "parse_imported",                                     :default => false
    t.boolean  "firebase_imported",                                  :default => false
    t.decimal  "price",               :precision => 10, :scale => 2
    t.boolean  "enable_purchases",                                   :default => false
  end

  add_index "posts", ["access_token"], :name => "index_posts_on_access_token", :unique => true
  add_index "posts", ["category_id"], :name => "index_posts_on_category_id"
  add_index "posts", ["college_id", "status", "category_id"], :name => "index_posts_on_college_id_and_status_and_category_id"
  add_index "posts", ["college_id", "status", "subcategory_id"], :name => "index_posts_on_college_id_and_status_and_subcategory_id"
  add_index "posts", ["college_id", "status"], :name => "index_posts_on_college_id_and_status"
  add_index "posts", ["college_id"], :name => "index_posts_on_college_id"
  add_index "posts", ["status"], :name => "index_posts_on_status"
  add_index "posts", ["subcategory_id"], :name => "index_posts_on_subcategory_id"

  create_table "scammers", :force => true do |t|
    t.string   "email",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "scammers", ["email"], :name => "index_scammers_on_email", :unique => true

  create_table "spammers", :force => true do |t|
    t.string   "email",      :null => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "spammers", ["email"], :name => "index_spammers_on_email", :unique => true

  create_table "subcategories", :force => true do |t|
    t.integer "category_id"
    t.string  "name"
  end

  create_table "sync_logs", :force => true do |t|
    t.integer  "payment_id"
    t.integer  "event_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "users", :force => true do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email"
    t.string   "password_hash"
    t.string   "password_salt"
    t.string   "user_type",     :default => "User"
    t.boolean  "disabled",      :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true

end
