class AddPriceToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :price, :decimal, :precision => 10, :scale => 2
  end

  def self.down
    remove_column :posts, :price
  end
end
