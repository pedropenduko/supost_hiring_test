class CreateMessages < ActiveRecord::Migration
  def self.up
    create_table :messages do |t|
      t.text    :message
      t.integer :post_id
      t.string  :ip
      t.string  :email

      t.timestamps
    end
  end

  def self.down
    drop_table :messages
  end
end
