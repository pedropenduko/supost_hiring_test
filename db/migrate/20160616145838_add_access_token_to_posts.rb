class AddAccessTokenToPosts < ActiveRecord::Migration
  def self.up
    add_column :posts, :access_token, :string
    add_index  :posts, :access_token, :unique => true

    say_with_time 'generating access token for posts' do
      Post.generate_access_tokens
    end
  end

  def self.down
    remove_column :posts, :access_token
  end
end
