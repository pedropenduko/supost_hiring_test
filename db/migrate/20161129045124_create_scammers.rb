class CreateScammers < ActiveRecord::Migration
  def self.up
    create_table :scammers do |t|
      t.string :email, :null => false

      t.timestamps
    end

    add_index :scammers, :email, :unique => true
  end

  def self.down
    drop_table :scammers
  end
end
