require 'securerandom'

class User < ActiveRecord::Base
  EMAIL_REGEX = /^[A-Z0-9._%-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i

  before_validation :remove_whitespaces!
  before_save :encrypt_password

  attr_accessible :first_name, :last_name, :email, :password, :password_confirmation, :disabled
  attr_accessor   :password

  validates_presence_of     :first_name
  validates_length_of       :first_name, :maximum => 255, :allow_nil => true, :allow_blank => true

  validates_presence_of     :last_name
  validates_length_of       :last_name, :maximum => 255, :allow_nil => true, :allow_blank => true

  validates_presence_of     :email
  validates_format_of       :email, :with => EMAIL_REGEX, :allow_nil => true, :allow_blank => true
  validates_length_of       :email, :maximum => 255, :allow_nil => true, :allow_blank => true
  validates_uniqueness_of   :email, :allow_nil => true, :allow_blank => true

  validates_presence_of     :password, :on => :create
  validates_length_of       :password, :in => 6..32, :allow_nil => true, :allow_blank => true
  validates_confirmation_of :password, :allow_nil => true, :allow_blank => true

  scope :active, :conditions => { :disabled => false }

  def reset_password!
    password = SecureRandom.hex(6)
    self.update_attributes!(:password => password, :password_confirmation => password)
    password
  end

  def can_update?(user)
    if self.new_record? || user.blank? || user.new_record?
      false
    elsif user.admin?
      false
    elsif self.id == user.id
      false
    elsif self.admin?
      true
    else
      false
    end
  end

  def admin?
    user_type == 'Admin'
  end

  def enabled?
    !disabled?
  end

  def name
    "#{first_name} #{last_name}".strip
  end

  def created_on
    created_at.strftime('%b %d, %Y')
  end

  def change_password(current, password_params)
    if new_record?
      errors.add :base, 'Cannot change password for new record'
      false
    elsif authenticate!(current)
      self.update_attributes password_params.slice(:password, :password_confirmation)
    else
      errors.add :base, 'Current password is invalid'
      false
    end
  end

  def authenticate!(password)
    self.password_hash == BCrypt::Engine.hash_secret(password, self.password_salt)
  end

  def self.authenticate(email, password)
    user = active.find_by_email(email)
    if user && user.authenticate!(password)
      user
    else
      nil
    end
  end

  private

  def remove_whitespaces!
    self.first_name = self.first_name.strip if self.first_name.respond_to?(:strip)
    self.last_name = self.last_name.strip if self.last_name.respond_to?(:strip)
    self.email = self.email.strip if self.email.respond_to?(:strip)
  end

  def encrypt_password
    if password.present?
      self.password_salt = BCrypt::Engine.generate_salt
      self.password_hash = BCrypt::Engine.hash_secret(self.password, self.password_salt)
    end
  end
end
