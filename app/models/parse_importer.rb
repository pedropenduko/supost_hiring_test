require 'rubygems'
require 'parse-ruby-client'

class ParseImporter
  MAX        = 500
  MIN        = 20
  BATCH_SIZE = 50

  cattr_reader :config
  @@config = YAML.load_file(Rails.root.join('config', 'parse.yml'))[Rails.env]

  class << self
    def setup_parse
      Parse.init(
        :application_id => config['application_id'],
        :api_key        => config['api_key']
      )
      Parse.client.session.insecure = true
      # Parse.client.session.enable_debug if Parse.client.session.respond_to?(:enable_debug)
    end

    def import_all
      puts "** Task: import_all, Class Name: #{config['class_name'].inspect}"
      Post.where(:parse_imported => false).find_in_batches do |records|
        records.in_groups_of(MAX, false).each do |posts|
          sync(posts)

          seconds = [60, 90].sample
          puts "-> Sleep #{seconds} seconds"
          sleep seconds
        end
      end
    end

    def import
      puts "** Task: import, Class Name: #{config['class_name'].inspect}"
      posts = Post.where(:parse_imported => false).order('time_modified DESC').limit(MAX).all
      sync(posts)
    end

    def update
      puts "** Task: update, Class Name: #{config['class_name'].inspect}"
      limit = get_limit
      posts = Post.limit(limit).order('time_modified DESC').all
      sync(posts)
    end

    def sync(posts)
      return if posts.blank?

      setup_parse

      post_ids = posts.map(&:id)
      posts    = posts.reduce({}) { |h, p| h.update(p.id => p) }

      puts "=> Query existing posts from parse.com"
      remote_posts = query_posts(post_ids)
      remote_posts.delete_if { |p| p['reference_id'].blank? }

      existing_post_ids = remote_posts.map { |p| p['reference_id'] } & post_ids
      new_post_ids      = post_ids - existing_post_ids

      if existing_post_ids.present?
        puts "=> Updating #{existing_post_ids.length} existing posts"

        count = 0
        remote_posts.in_groups_of(BATCH_SIZE, false).each do |grouped_remote_posts|
          batch = Parse::Batch.new

          updated_post_ids = grouped_remote_posts.map do |remote_post|
            pid = remote_post['reference_id']
            posts[pid].parse_attributes.each { |key, value| remote_post[key] = value }
            batch.update_object(remote_post)
            pid
          end

          batch.run!

          updated_post_ids.each do |pid|
            post = posts[pid]
            post.sudo { |p| p.mark_as_parse_imported! }
          end

          count += grouped_remote_posts.size
          puts "-> Updated #{count}/#{remote_posts.size} Posts"
        end
      end

      if new_post_ids.present?
        puts "=> Importing #{new_post_ids.length} non-existing posts"

        count = 0
        new_post_ids.in_groups_of(BATCH_SIZE, false).each do |grouped_post_ids|
          batch = Parse::Batch.new

          grouped_post_ids.each do |pid|
            attributes = posts[pid].parse_attributes
            remote_post = Parse::Object.new config['class_name'], attributes
            batch.create_object(remote_post)
          end

          batch.run!

          grouped_post_ids.each do |pid|
            post = posts[pid]
            post.sudo { |p| p.mark_as_parse_imported! }
          end

          count += grouped_post_ids.size
          puts "-> Imported #{count}/#{new_post_ids.size} Posts"
        end
      end
    rescue Exception => ex
      print_error ex
    end

    def query_posts(post_ids)
      query       = Parse::Query.new(config['class_name'])
      query.limit = post_ids.size

      if post_ids.size > 1
        query.order    = :descending
        query.order_by = 'reference_id'
      end

      query.value_in('reference_id', post_ids)

      query.get
    end

    def print_error(ex)
      puts "** ERROR: #{ex.message}"
      puts ex.backtrace
    end

    def get_limit
      limit = (ENV['NUM'] ? ENV['NUM'].to_i : MIN)

      if limit > MAX
        limit = MAX
      elsif limit < MIN
        limit = MIN
      end

      limit
    end
  end
end
