require 'rubygems'
require 'firebase'

class FirebaseImporter
  MAX        = 500
  MIN        = 20
  BATCH_SIZE = 50

  class << self
    def import_all
      puts "** Task: import_all"
      client = new_client
      Post.where(:firebase_imported => false).find_in_batches do |posts|
        sync(posts, client)
      end
    end

    def import
      puts "** Task: import"
      client = new_client
      posts = Post.where(:firebase_imported => false).order('time_modified DESC').limit(MAX).all
      sync(posts, client)
    end

    def update
      puts "** Task: update"
      limit = get_limit
      posts = Post.limit(limit).order('time_modified DESC').all
      sync(posts)
    end

    def sync(posts, client = nil)
      return if posts.blank?

      client ||= new_client

      count = 0
      posts.in_groups_of(BATCH_SIZE, false) do |grouped_posts|
        data = grouped_posts.reduce({}) do |hash, post|
          hash.update(post.firebase_key => post.firebase_attributes)
        end

        res = client.update('', data)

        if res.success?
          grouped_posts.each do |post|
            post.sudo { |p| p.mark_as_firebase_imported! }
          end

          count += grouped_posts.size
          puts "-> SYNCED #{count}/#{posts.size} POSTS"
        else
          puts "-> FAILED TO IMPORT POSTS #{grouped_posts.map(&:id)}"
        end
      end
    end

    def import_categories_and_subcategories
      client ||= new_client

      puts "** CATEGORIES **"

      count = 0
      categories = Category.all
      categories.in_groups_of(BATCH_SIZE, false) do |grouped_categories|
        data = grouped_categories.reduce({}) do |hash, category|
          key = "categories/#{category.id}"
          hash.update(key => category.attributes)
        end

        res = client.update('', data)

        if res.success?
          count += grouped_categories.size
          puts "-> SYNCED #{count}/#{categories.size} CATEGORIES"
        else
          puts "-> FAILED TO IMPORT CATEGORIES #{grouped_categories.map(&:id)}"
        end
      end

      puts "** SUBCATEGORIES **"

      count = 0
      subcategories = Subcategory.all
      subcategories.in_groups_of(BATCH_SIZE, false) do |grouped_subcategories|
        data = grouped_subcategories.reduce({}) do |hash, subcategory|
          key = "subcategories/#{subcategory.id}"
          hash.update(key => subcategory.attributes)
        end

        res = client.update('', data)

        if res.success?
          count += grouped_subcategories.size
          puts "-> SYNCED #{count}/#{subcategories.size} SUBCATEGORIES"
        else
          puts "-> FAILED TO IMPORT SUBCATEGORIES #{grouped_subcategories.map(&:id)}"
        end
      end
    end

    def new_client
      config = YAML.load_file(Rails.root.join('config', 'firebase.yml'))[Rails.env]
      Firebase::Client.new(config['database_url'], config['database_secret'])
    end

    def get_limit
      limit = (ENV['NUM'] ? ENV['NUM'].to_i : MIN)

      if limit > MAX
        limit = MAX
      elsif limit < MIN
        limit = MIN
      end

      limit
    end
  end
end
