class Post < ActiveRecord::Base
  include EmailValidationHelper

  COLLEGE_ID_STANFORD = 1

  # Post status
  CREATED   = -1
  CONFIRMED = 0
  ACTIVE    = 1
  DELETED   = 2
  RESERVED  = 3
  PURCHASED = 4

  STATUS_NAMES = {
    CREATED   => 'New',
    CONFIRMED => 'Confirmed',
    ACTIVE    => 'Active',
    DELETED   => 'Deleted',
    RESERVED  => 'Reserved',
    PURCHASED => 'Purchased'
  }.freeze

  FILTER_STATUS_NAMES = [].tap do |ary|
    ary << ['All', '']
    STATUS_NAMES.each { |k, v| ary << [v, k.to_s] }
    ary << ['Reserved or Purchased', [RESERVED, PURCHASED].join(',')]
  end

  FILTER_FIELDS = [
    ['Email', 'email'],
    ['Name', 'name'],
    ['ID', 'id']
  ]

  DIRECTORY_RAW    = File.join("public", "uploads", "raw")
  DIRECTORY_POST   = File.join("public", "uploads", "post")
  DIRECTORY_TICKER = File.join("public", "uploads", "ticker")

  IMAGE_SOURCE_FIELDS = [:image_source1, :image_source2, :image_source3, :image_source4]
  PHOTO_FIELDS        = [:photo1, :photo2, :photo3, :photo4]
  PHOTO_NAME_FIELDS   = [:photo1_file_name, :photo2_file_name, :photo3_file_name, :photo4_file_name]

  EMAIL_REGEX = /^[A-Z0-9._%-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i

  ALLOWED_DOMAINS = %w(
    stanford.edu
    stanfordalumni.org
    stanfordhealthcare.org
    stanfordmed.org
    lpch.org
  ).map { |domain| "(#{domain.gsub('.', '\.')}$)" }.join('|')

  AUTHENTICATED_EMAIL_REGEX = /#{ALLOWED_DOMAINS}/i

  PAPERCLIP_OPTIONS = {
    :storage        => :s3,
    :s3_credentials => "#{Rails.root}/config/amazon_s3.yml",
    :default_url    => '/images/site/missing.png',
    :default_style  => :post,
    :url            => ":s3_domain_url",
    :use_timestamp  => true,
    :styles         => {
      :post   => '340x510>', # width x height
      :ticker => '300x120>'
    }
  }

  BUYABLE_MAX_PRICE     = 50
  PURCHASE_FEE_RATE     = 0.14 # 14%
  RESERVATION_FEE_RATE  = 0.20 # 20%

  attr_accessor :created_by_admin

  has_many :messages
  has_many :payments

  belongs_to :category
  belongs_to :subcategory

  has_attached_file :photo1, PAPERCLIP_OPTIONS.merge(:path => 'posts/:id/:style_:ida')
  has_attached_file :photo2, PAPERCLIP_OPTIONS.merge(:path => 'posts/:id/:style_:idb')
  has_attached_file :photo3, PAPERCLIP_OPTIONS.merge(:path => 'posts/:id/:style_:idc')
  has_attached_file :photo4, PAPERCLIP_OPTIONS.merge(:path => 'posts/:id/:style_:idd')

  scope :latest,         :order      => 'time_posted DESC'
  scope :active,         :conditions => { :college_id => COLLEGE_ID_STANFORD, :status => ACTIVE }
  scope :have_photos,    :conditions => IMAGE_SOURCE_FIELDS.map { |f| "(#{f} IS NOT NULL)" }.join(" OR ")
  scope :for_job,        :conditions => { :category_id => Category::OFF_CAMPUS_JOBS }
  scope :by_category,    lambda { |category|    { :conditions => { :category_id => category } } }
  scope :by_subcategory, lambda { |subcategory| { :conditions => { :subcategory_id => subcategory } } }
  scope :have_text,      lambda { |query|       { :conditions => [ 'name LIKE ? OR body LIKE ?', "%#{query}%", "%#{query}%" ] } }

  before_validation :preprocess_fields!
  before_create     :add_extra_information

  after_photo1_post_process :copy_photo_1
  after_photo2_post_process :copy_photo_2
  after_photo3_post_process :copy_photo_3
  after_photo4_post_process :copy_photo_4

  validates :name,
            :presence => true,
            :length => { :maximum => 255, :allow_blank => true }

  validates :price,
            :presence => { :if => :price_required? },
            :numericality => {
              :greater_than_or_equal_to => 0,
              :less_than_or_equal_to => 1_000_000,
              :allow_nil => true,
              :if => :price_required?
            }

  validates :body,
            :presence => true,
            :length => { :maximum => 32_000, :allow_blank => true }

  validates_email
  validates_format_of :email,
                      :with => AUTHENTICATED_EMAIL_REGEX,
                      :message => 'must be a Stanford email (e.g., @stanford.edu, @cs.stanford.edu, @gsb.stanford.edu, @stanfordalumni.org).',
                      :if => :authenticated_email_required?

  delegate :short_name, :name, :to => :category, :prefix => true, :allow_nil => true
  delegate :name, :to => :subcategory, :prefix => true, :allow_nil => true

  def stripe_metadata
    {
      :post_id       => id,
      :name          => name,
      :seller        => email,
      :price         => price_in_cents,
      :paid          => stripe_amount,
      :checkout_type => stripe_checkout_type
    }
  end

  def display_checkout_type
    if purchasable?
      :purchase
    else
      :reservation
    end
  end

  def stripe_checkout_type
    if purchasable?
      :purchase
    elsif reservable?
      :reservation
    else
      :unknown
    end
  end

  def stripe_description
    if purchasable?
      "Purchase: #{name}"
    elsif reservable?
      "Reservation: #{name}"
    else
      name
    end
  end

  def stripe_amount
    if purchasable?
      purchase_price_in_cents
    elsif reservable?
      reserve_price_in_cents
    else
      0
    end
  end

  def rate
    if purchasable?
      PURCHASE_FEE_RATE
    elsif reservable?
      RESERVATION_FEE_RATE
    else
      0
    end
  end

  def fee_percentage
    percent = rate * 100
    PriceHelper.round percent.round(2)
  end

  def mail_subject
    if purchasable?
      "SUpost - Purchase - #{name}"
    elsif reservable?
      "SUpost - Reservation - #{name}"
    else
      name
    end
  end

  def on_sale?
    active? && within_2_months? && (purchasable? || reservable?)
  end

  def buyable?
    price.present? && price > 0 && price <= BUYABLE_MAX_PRICE
  end

  def purchasable?
    enable_purchases? && buyable? 
  end

  def reservable?
    (!enable_purchases? && buyable?) ||
      (price.present? && price > BUYABLE_MAX_PRICE)
  end

  def price_in_cents
    PriceHelper.to_cents(rounded_price)
  end

  def purchase_price
    amount = rounded_price * (1 + PURCHASE_FEE_RATE)
    PriceHelper.round amount.round(2)
  end

  def purchase_price_in_cents
    PriceHelper.to_cents(purchase_price)
  end

  def reserve_price
    amount = rounded_price * RESERVATION_FEE_RATE
    PriceHelper.round amount.round(2)
  end

  def reserve_price_in_cents
    PriceHelper.to_cents(reserve_price)
  end

  def rounded_fee
    fee = rounded_price * rate
    PriceHelper.round fee.round(2)
  end

  def rounded_price
    @rounded_price ||= PriceHelper.round(price)
  end

  def display_price
    return nil unless have_price?
    rounded_price
  end

  def price_required?
    (category.try(:price_required?) && subcategory.try(:price_required?)) || false
  end

  def have_price?
    price.present? && price_required?
  end

  def for_off_campus_jobs?
    category.try(:off_campus_jobs?) || false
  end

  def for_housing_offering?
    category.try(:housing_offering?) || false
  end

  def update_and_save_photos(post_params, raw_photos)
    update_attributes(post_params) && save_with_photos(raw_photos)
  end

  def save_with_photos(raw_photos)
    return false unless valid? && self.save

    PHOTO_FIELDS.each_with_index do |photo_field, idx|
      if photo = raw_photos[photo_field]
        image_field = IMAGE_SOURCE_FIELDS[idx]

        suffix = (idx + 97).chr # a/b/c/d
        photo_name = "#{self.id}#{suffix}"
        self[image_field] = photo_name

        self.send("#{photo_field}=", photo)
        photo_name_field = PHOTO_NAME_FIELDS[idx]
        self.send("#{photo_name_field}=", photo_name)
      end
    end

    self.time_modified = Time.now.to_i
    self.save
  end

  def send_activation_mail!
    if Rails.env.development?
      send_activation_mail_now!
    else
      send_activation_mail_later!
    end
  end

  def send_activation_mail_now!
    Notifier.activate(self).deliver
  end

  def send_activation_mail_later!
    Notifier.delay.activate(self.id)
  end

  def enqueue_jobs!
    Delayed::Job.enqueue ParseJob.new(self.id),    :queue => 'parse'
    Delayed::Job.enqueue FirebaseJob.new(self.id), :queue => 'firebase'
  end

  def generate_access_token
    return if self.access_token.present?
    self.access_token = random_access_token
    while self.class.exists?(:access_token => self.access_token)
      self.access_token = random_access_token
    end
  end

  def generate_access_token!
    generate_access_token
    save!
  end

  def sudo(&block)
    begin
      self.created_by_admin = true
      yield self
    ensure
      self.created_by_admin = false
    end
  end

  def mark_as_confirmed!
    change_to_status(CONFIRMED)
  end

  def mark_as_active!
    change_to_status(ACTIVE)
    enqueue_jobs!
  end

  def mark_as_deleted!
    change_to_status(DELETED)
    enqueue_jobs!
  end

  def mark_as_reserved!
    change_to_status(RESERVED)
    enqueue_jobs!
  end

  def mark_as_purchased!
    change_to_status(PURCHASED)
    enqueue_jobs!
  end

  def created?
    status == CREATED
  end

  def confirmed?
    status == CONFIRMED
  end

  def active?
    status == ACTIVE
  end

  def deleted?
    status == DELETED
  end

  def reserved?
    status == RESERVED
  end

  def purchased?
    status == PURCHASED
  end

  def reserved_or_purchased?
    reserved? || purchased?
  end

  def inactive?
    !active?
  end

  def ready?
    active? || deleted?
  end

  def status_name
    STATUS_NAMES[status || CREATED]
  end

  def code
    @code ||= (category || Category.new).code
  end

  def full_name
    "Post #{name.inspect} (#{id})"
  end

  def truncated_name(length = 24)
    truncated_field name, length
  end

  def truncated_email(length = 20)
    truncated_field email, length
  end

  def display_posted_date
    posted_at.strftime("%a, %b %d")
  end

  def display_posted_at
    posted_at.strftime("%a, %b %d, %Y %I:%M %p")
  end

  def formatted_posted_at
    posted_at.strftime("%m/%d/%Y %I:%M %p")
  end

  def within_2_months?
    posted_at > (Date.today - 2.months)
  end

  def posted_at
    Time.at(time_posted || created_at)
  end

  def ticker_photo
    photo = photos.first
    return unless photo
    photo.url(:ticker)
  end

  def local_ticker_photo_url(name)
    File.join('', 'uploads', 'ticker', name)
  end

  def post_photos
    photos.map { |photo| photo.url(:post) }
  end

  def photos
    @photos ||= PHOTO_FIELDS.map { |field| self.send(field) }.select(&:present?)
  end

  def have_photos?
    photos.present?
  end

  def photo1_url
    photo1.present? ? photo1.url : nil
  end

  def photo2_url
    photo2.present? ? photo2.url : nil
  end

  def photo3_url
    photo3.present? ? photo3.url : nil
  end

  def photo4_url
    photo4.present? ? photo4.url : nil
  end

  def photo1_updated_at
    updated_at
  end

  def photo2_updated_at
    updated_at
  end

  def photo3_updated_at
    updated_at
  end

  def photo4_updated_at
    updated_at
  end

  def parse_attributes
    attrs = attributes.slice(
      'college_id',
      'category_id',
      'subcategory_id',
      'name',
      'body',
      'email',
      'security_id',
      'ip',
      'status',
      'time_posted',
      'time_modified'
    )
    attrs.merge!(
      'reference_id'     => id,
      'image_source1'    => photo1_file_name,
      'image_source2'    => photo2_file_name,
      'image_source3'    => photo3_file_name,
      'image_source4'    => photo4_file_name,
      'price'            => price,
      'enable_purchases' => enable_purchases?
    )
    (1..4).each do |idx|
      field = "image_source#{idx}"
      attrs.delete(field) if attrs[field].blank?
    end
    attrs
  end

  def mark_as_parse_imported!
    self.parse_imported = true
    self.save(:validate => false)
  end

  def firebase_attributes
    attrs = attributes.slice(
      'id',
      'college_id',
      'category_id',
      'subcategory_id',
      'name',
      'body',
      'email',
      'status',
      'time_posted',
      'time_modified'
    )
    attrs.merge(
      'image_source1'    => photo1_url || '',
      'image_source2'    => photo2_url || '',
      'image_source3'    => photo3_url || '',
      'image_source4'    => photo4_url || '',
      'price'            => price,
      'enable_purchases' => enable_purchases?
    )
  end

  def firebase_key
    "posts/#{id}"
  end

  def mark_as_firebase_imported!
    self.firebase_imported = true
    self.save(:validate => false)
  end

  def self.recent_posts
    active.latest.all(:limit => 50)
  end

  def self.recent_photo_posts
    active.latest.have_photos.all(:limit => 4)
  end

  def self.recent_job_posts
    active.latest.for_job.all(:limit => 3)
  end

  def self.filter(options = {})
    status, field, value = options.values_at(:status, :field, :value)

    arel = latest

    if status.present?
      status_ary = status.split(',').map(&:to_i).select { |s| STATUS_NAMES[s] }
      arel = arel.where(:status => status_ary) if status_ary.present?
    end

    value.try(:strip!)
    if (field == 'email' || field == 'name') && value.present?
      arel = arel.where("LOWER(#{field}) LIKE ?", "%#{value.downcase}%")
    elsif field == 'id' && value.to_i > 0
      arel = arel.where(:id => value)
    end

    arel
  end

  def self.search(query, category_id, subcategory_id, page)
    query.respond_to?(:strip!) && query.strip!
    query.respond_to?(:gsub!)  && query.gsub!(/\\/, '\&\&')
    query.respond_to?(:gsub!)  && query.gsub!(/'/, "''")

    arel = self.active.latest
    if query.present?
      arel = arel.have_text(query)
    elsif subcategory_id
      arel = arel.by_subcategory(subcategory_id)
    elsif category_id
      if category_id.to_i == Category::HOUSING_OFFERING
        arel = arel.by_category([category_id, Category::HOUSING_NEED])
      else
        arel = arel.by_category(category_id)
      end
    end
    arel.page(page).per(100)
  end

  def self.category_activity_metrics
    result = {}

    Category.active_category_ids.each do |category|
      post = self.active.by_category(category).latest.first
      next if post.blank?
      result[category] = post.posted_at
    end

    result
  end

  def self.generate_access_tokens
    self.where(:access_token => nil).find_each(:batch_size => 50000) do |post|
      post.generate_access_token
      self.where(:id => post.id).update_all :access_token => post.access_token
    end
  end

  def self.new_job_post(params = {})
    options = {
      :category_id    => Category::OFF_CAMPUS_JOBS,
      :subcategory_id => Subcategory::OFF_CAMPUS_JOBS_GENERAL
    }
    options = options.merge(params) if params
    new_with_default_values(options)
  end

  def self.new_with_default_values(params = {})
    time = Time.now.to_i
    options = {
      :college_id    => COLLEGE_ID_STANFORD,
      :status        => CREATED,
      :security_id   => rand(100_000),
      :time_posted   => time,
      :time_modified => time
    }
    options = options.merge(params) if params
    new(options)
  end

  def self.populate(num = 10)
    require 'ffaker'

    FileUtils.mkdir_p File.join(Rails.root, DIRECTORY_RAW)
    FileUtils.mkdir_p File.join(Rails.root, DIRECTORY_POST)
    FileUtils.mkdir_p File.join(Rails.root, DIRECTORY_TICKER)

    dates         = (-10..0).map { |date| Time.now + date.days }
    prices        = (10..200).to_a
    categories    = Category.all
    subcategories = Subcategory.all.group_by(&:category_id)

    1.upto(num) do |i|
      category    = nil
      subcategory = nil

      while !category || !subcategory do
        category    = categories.sample
        redo if category.inactive?
        subcategory = Array(subcategories[category.id]).sample
      end

      time = dates.sample.to_i

      options = {
        :status        => ACTIVE,
        :email         => "#{FFaker::Internet::user_name}@stanford.edu",
        :name          => FFaker::Lorem.sentence,
        :body          => FFaker::Lorem.paragraphs,
        :category      => category,
        :subcategory   => subcategory,
        :time_posted   => time,
        :time_modified => time
      }

      if category.price_required?
        options.merge!(
          :price            => prices.sample,
          :enable_purchases => [false, true, true].sample,
        )
      end

      photos = {}
      if !category.off_campus_jobs? && [false, true, true].sample
        photos[:photo1] = File.open(File.join(Rails.root, 'public', 'images', 'site', 'seacoast.jpg'))
      end

      post = self.new_with_default_values(options)
      post.save_with_photos photos
      print "\rPopulating #{i}/#{num}... "
    end
    puts "Done!"
  end

  private

  def authenticated_email_required?
    !created_by_admin
  end

  def change_to_status(status)
    self.status = status
    self.time_modified = Time.now.to_i
    self.save(:validate => false)
  end

  def copy_photo_1
    if self.photo1.present?
      copy_photo self.photo1, self.image_source1
    end
  end

  def copy_photo_2
    if self.photo2.present?
      copy_photo self.photo2, self.image_source2
    end
  end

  def copy_photo_3
    if self.photo3.present?
      copy_photo self.photo3, self.image_source3
    end
  end

  def copy_photo_4
    if self.photo4.present?
      copy_photo self.photo4, self.image_source4
    end
  end

  def copy_photo(photo, photo_file_name)
    queued_for_write = photo.instance_variable_get(:@queued_for_write)
    queued_for_write.slice(:post, :ticker).each do |style, file|
      new_path = File.join(Rails.root, 'public', 'uploads', style.to_s, photo_file_name)
      FileUtils.cp file.path, new_path
      FileUtils.chmod 0644, new_path
    end
  end

  def truncated_field(str, length)
    if str.present?
      chars = str.mb_chars
      if chars.length <= length
        str
      else
        chars[0...length] + '...'
      end
    else
      ''
    end
  end

  def preprocess_fields!
    self.name = self.name.strip if self.name.respond_to?(:strip)
    self.email = self.email.strip if self.email.respond_to?(:strip)
    if price_required? && self.price
      self.price = self.price.to_f.round(2)
    else
      self.price = nil
    end
  end

  def add_extra_information
    now = Time.now.to_i
    self.college_id    = COLLEGE_ID_STANFORD if self.college_id.blank?
    self.status        = CREATED if self.status.blank?
    self.time_posted   = now if self.time_posted.blank?
    self.time_modified = now if self.time_modified.blank?
    self.security_id   = rand(100_000) if self.security_id.blank?
    generate_access_token
  end

  def random_access_token
    SecureRandom.hex(32)
  end
end
