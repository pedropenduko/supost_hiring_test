class Event < ActiveRecord::Base
  serialize :data, Hash

  has_many :sync_logs
  has_many :payments, :through => :sync_logs

  scope :recent, order('id DESC')

  def created_time
    Time.at(created) rescue created_at
  end

  def display_created_time(fmt = '%m/%d/%Y %I:%M:%S %p')
    created_time.strftime(fmt)
  end

  def charge_succeeded?
    stripe_type == 'charge.succeeded'
  end

  def data_object
    Hash === data ? data['object'] || {} : {}
  end

  def self.create_event!(params)
    event_params = to_event_params(params)
    create!(event_params)
  end

  private

  def self.to_event_params(params)
    {
      :stripe_id        => params['id'],
      :stripe_object    => params['object'],
      :stripe_type      => params['type'],
      :api_version      => params['api_version'],
      :created          => params['created'],
      :live_mode        => params['live_mode'],
      :pending_webbooks => params['pending_webbooks'],
      :request          => params['request'],
      :data             => params['data']
    }
  end
end
