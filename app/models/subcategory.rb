class Subcategory < ActiveRecord::Base
  MISC_CATEGORY           = 158
  RESUMES_GENERAL         = 0
  OFF_CAMPUS_JOBS_GENERAL = 158

  FOR_SALE_ITEMS_WANTED   = 18

  APT_HOUSING_WANTED      = 68
  ROOM_SHARE_WANTED       = 152
  SUBLET_TEMP_WANTED      = 153

  belongs_to :category

  def items_wanted?
    id == FOR_SALE_ITEMS_WANTED
  end

  def house_wanted?
    id == APT_HOUSING_WANTED ||
      id == ROOM_SHARE_WANTED ||
      id == SUBLET_TEMP_WANTED
  end

  def price_required?
    !items_wanted? && !house_wanted?
  end
end
