class SiteController < ApplicationController
  caches_page :index
    
  def index
    @google_ad_manager = true
    @posts             = Post.recent_posts
    @photo_posts       = Post.recent_photo_posts
    @job_posts         = Post.recent_job_posts
    @time_ago          = Post.category_activity_metrics
  end

  def help
  end

  def about
  end

  def contact
  end

  def privacy
  end

  def terms
  end

  def safety
    redirect_to(safety_link) and return
  end

  def state
    no_cache_now
    render :text => 'OK!', :status => 200
  end

  def robots
    render :file => File.join(Rails.root, 'config', 'robots.txt'), :layout => false, :content_type => "text/plain"
  end

  private

  def safety_link
    "http://www.craigslist.org/about/scams"
  end
end
