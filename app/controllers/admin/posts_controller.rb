class Admin::PostsController < AdminController
  before_filter :find_post, :only => [:show, :edit, :update, :readonly, :notify, :publish, :release, :destroy, :similar, :report]
  before_filter :check_if_post_can_be_updated?, :only => [:show, :edit, :update]

  helper_method :can_update_post?

  def index
    set_default_finder_options
    @posts = paginate Post.filter(finder_options)
  end

  def new
    @post = Post.new_job_post
    load_categories_and_subcategories(@post)
  end

  def create
    @post = Post.new_job_post(job_post_params)
    @post.ip = request_ip

    if @post.sudo { |post| post.save_with_photos(raw_photos) }
      @post.enqueue_jobs!
      @post.send_activation_mail! if params[:notify_email] == 'true'
      expire_home_page
      redirect_to admin_posts_url, :notice => 'New post was created successfully!'
    else
      load_categories_and_subcategories(@post)
      render :action => 'new'
    end
  end

  def show
    load_categories_and_subcategories(@post)
    render :action => 'edit'
  end

  def edit
    load_categories_and_subcategories(@post)
  end

  def update
    if @post.sudo { |post| post.update_and_save_photos(job_post_params, raw_photos) }
      @post.enqueue_jobs!
      expire_home_page
      notice = "#{@post.full_name} was updated successfully!"
      if @post.for_off_campus_jobs?
        redirect_to job_admin_posts_url, :notice => notice
      else
        redirect_to admin_posts_url, :notice => notice
      end
    else
      load_categories_and_subcategories(@post)
      render :action => 'edit'
    end
  end

  def readonly
    @disable_message_box = true
  end

  def notify
    @post.send_activation_mail!
    redirect_to request.referrer || admin_posts_url, :notice => "Publish link of #{@post.full_name} was sent to #{@post.email.inspect} successfully!"
  end

  def publish
    if @post.inactive?
      notice = "#{@post.full_name} was changed to #{'Active'.inspect} status successfully!"
      @post.mark_as_active!
    else
      notice = "#{@post.full_name} was changed to #{'Deleted'.inspect} status successfully!"
      @post.mark_as_deleted!
    end
    expire_home_page
    redirect_to request.referrer || admin_posts_url, :notice => notice
  end

  def release
    notice = "#{@post.full_name} was changed to #{'Active'.inspect} status successfully!"
    @post.mark_as_active!
    expire_home_page
    redirect_to request.referrer || admin_posts_url, :notice => notice
  end

  def destroy
    if can_delete_post?(@post)
      notice = "#{@post.full_name} was destroyed successfully!"
      @post.destroy
    else
      notice = "Cannot delete #{@post.full_name}!"
    end
    redirect_to request.referrer || admin_posts_url, :notice => notice
  end

  def job
    @posts = paginate Post.for_job.filter(finder_options)
  end

  def similar
    new_finder_options = params.slice(:status).merge(:field => 'email', :value => @post.email)
    @posts = paginate Post.filter(new_finder_options)
  end

  def report
    spammer = Spammer.new(:email => @post.email)
    if spammer.save
      options = { :notice => "New spammer #{spammer.email.inspect} was added successfully!" }
    else
      options = { :alert => "Spammer #{spammer.email.inspect} existed!" }
    end
    redirect_to admin_spammers_url, options
  end

  private

  def find_post
    @post = Post.find params[:id]
  end

  def check_if_post_can_be_updated?
    redirect_to admin_posts_url and return unless can_update_post?(@post)
  end

  def can_update_post?(post)
    post.for_off_campus_jobs?
  end

  def can_delete_post?(post)
    post.created? && !post.for_off_campus_jobs?
  end

  def paginate(posts)
    posts.includes(:category, :subcategory).page(params[:page]).per(20)
  end

  def load_categories_and_subcategories(post)
    @categories = Category.all.map { |c| [c.short_name, c.id] }
    @grouped_subcategories = Hash.new { |h, k| h[k] = []  }
    Subcategory.all.each { |s| @grouped_subcategories[s.category_id] << [s.name, s.id] }
    @subcategories = @grouped_subcategories[ post.category_id ]
  end

  def post_params
    params[:post].slice(:category_id, :subcategory_id, :name, :email, :body, :status)
  end

  def job_post_params
    params[:post].slice(:name, :email, :body, :status)
  end

  def raw_photos
    params[:post].slice(:photo1, :photo2, :photo3, :photo4)
  end

  def set_default_finder_options
    params[:status] = '' if params[:status].blank?
    params[:field]  = 'email' if params[:field].blank?
    params[:value]  = '' if params[:value].blank?
  end

  def finder_options
    params.slice(:status, :field, :value)
  end
end
