class Admin::MessagesController < AdminController
  before_filter :find_post, :only => [:index]
  before_filter :find_message, :only => [:similar, :report]

  def index
    load_messages
  end

  def similar
    @messages = Message.recent_messages(@message.email, params[:page])
  end

  def report
    scammer = Scammer.new(:email => @message.email)
    if scammer.save
      options = { :notice => "New scammer #{scammer.email.inspect} was added successfully!" }
    else
      options = { :alert => "Scammer #{scammer.email.inspect} existed!" }
    end
    redirect_to admin_scammers_url, options
  end

  private

  def find_message
    @message = Message.find(params[:id])
  end

  def find_post
    @post = Post.find params[:post_id] if params[:post_id]
  end

  def load_messages
    @messages = @post.try(:messages) || Message
    @messages = @messages.recent.page(params[:page]).per(20)
  end
end
