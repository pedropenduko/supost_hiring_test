class StripeController < ApplicationController
  protect_from_forgery :except => :webhook

  def charge
    post = Post.find params[:post_id]
    redirect_to root_url and return unless post.on_sale?

    service = PostCheckoutService.new(post, stripe_params)
    service.checkout

    expire_home_page

    flash[:stripe_payment] = service.payment.try(:id)
    redirect_to post_url(post)
  rescue Stripe::CardError, Stripe::InvalidRequestError, StandardError => ex
    notify_exception(ex)
    flash[:stripe_error] = ex.message
    redirect_to post_url(post)
  end

  def webhook
    event = Event.create_event!(params)
    Payment.sync_data!(event)
    head :ok
  end

  private

  def stripe_params
    params.slice(:stripeEmail, :stripeToken, :stripeTokenType)
  end
end
