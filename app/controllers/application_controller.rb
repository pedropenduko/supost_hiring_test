class ApplicationController < ActionController::Base
  protect_from_forgery

  helper_method :request_ip

  protected

  def request_ip
    request.remote_ip || request.ip
  end

  def expire_home_page
    expire_page root_path
  end

  def no_cache_now
    response.headers["Cache-Control"] = "no-cache, no-store, max-age=0, must-revalidate"
    response.headers["Pragma"]        = "no-cache"
    response.headers["Expires"]       = "Fri, 01 Jan 1990 00:00:00 GMT"
  end

  def notify_exception(exception)
    log_error(exception)
    Rollbar.error(exception) if defined?(Rollbar)
  end

  def log_error(exception)
    return unless logger
    ActiveSupport::Deprecation.silence do
      message = "\n#{exception.class} (#{exception.message}):\n"
      message << '  ' << exception.backtrace.join("\n  ")
      logger.fatal("#{message}\n\n")
    end
  end

  def render_404
    render :file => File.join(Rails.root, 'public', '404.html'), :layout => false, :status => 404
  end

  def render_500
    render :file => File.join(Rails.root, 'public', '500.html'), :layout => false, :status => 500
  end
end
