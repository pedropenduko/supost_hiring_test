module AdminHelper
  def details_tag(content, options = {})
    options = { :class => 'details' }.merge(options)
    content_tag :div, post_body_tag(content), options
  end
end
