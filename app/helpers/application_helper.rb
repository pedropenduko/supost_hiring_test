module ApplicationHelper
  def sum_app_link
    "https://itunes.apple.com/us/app/sum-supost-mobile/id590013767?mt=8"
  end

  def safety_link
    "http://www.craigslist.org/about/scams"
  end

  def stanford_rank_link
    "http://grad-schools.usnews.rankingsandreviews.com/best-graduate-schools/top-engineering-schools/computer-engineering-rankings"
  end

  def employer_sign_up_link
    "https://supost.chargify.com/h/33114/subscriptions/new"
  end

  def trial_sign_up_link
    "https://supost.chargify.com/h/73028/subscriptions/new"
  end

  def buy_job_post_link
    "http://jobs.suposts.com"
  end

  def short_time_ago(time)
    str = ''
    if time.present?
      str = time_ago_in_words(time)
      str.sub!('less than a minute', '1 minute')
      str.sub!('about ', '')
      str.sub!('months', 'mons')
      str.sub!('seconds', 'secs')
      str.sub!('minutes', 'mins')
    end
    str
  end

  def show_breadcrumb(post = @post, category = @category, subcategory = @subcategory)
    html = []

    if post.present?
      category = post.category if category.blank? && post.category
      subcategory = post.subcategory if subcategory.blank? && post.subcategory
    end

    if subcategory
      html << ' &raquo; '
      html << link_to(subcategory.category.short_name, search_by_cat_path(subcategory.category.id))
      html << ' &raquo; '
      html << link_to(subcategory.name, search_by_sub_path(subcategory.id))
    elsif category
      html << ' &raquo; '
      html << link_to(category.short_name, search_by_cat_path(category.id))
    end

    if post.present? && !post.new_record?
      html << ' &raquo; '
      html << h(post.truncated_name(24))
    end

    html.join('').html_safe
  end

  def pretty_json(json)
    json = json.to_json if json.respond_to?(:to_json)
    hash = JSON.parse(json) rescue {}
    text = JSON.pretty_generate(hash)
    content_tag :pre, content_tag(:code, text, :class => 'json')
  end

  def post_title_tag(post)
    html = link_to post_display_name(post), post_path(post.id), :class => 'post-link'
    html << ' '
    html << post_camera_tag(post)
    html.html_safe
  end

  def post_display_name(post)
    name = [post.name]
    name << post_display_price(post) if post.have_price?
    name = name.join(' - ')
    h(name.strip)
  end

  def post_display_price(post)
    if post.price == 0
      'Free'
    else
      price_tag(post.rounded_price)
    end
  end

  def price_tag(price)
    number_to_currency(price, :precision => price.is_a?(Integer) ? 0 : 2)
  end

  def stripe_price_description(post)
    if post.purchasable?
      "Total (fee incl.): &nbsp;#{price_tag(post.purchase_price)}".html_safe
    else
      [
        "Total: &nbsp;#{price_tag(post.rounded_price + post.rounded_fee)}.",
        "Due later: &nbsp;#{price_tag post.rounded_price}"
      ].join("&nbsp;&nbsp;").html_safe
    end
  end

  def stripe_checkout_link(post)
    # TODO: Temporarily disable Stripe button link
    return

    return unless post.on_sale?

    options = {
      :class => 'stripe-button-el',
      :style => 'visibility: visible;'
    }

    if post.purchasable?
      text = 'Buy'
    else
      text = "Reserve for #{price_tag(post.reserve_price)}"
    end

    span = content_tag(:span, text, :style => 'display: block; min-height: 30px;')
    link_to span, post_path(post, :do => 'pay'), options
  end

  def stripe_checkout_button(post)
    return unless post.on_sale?

    options = {
      :src                => 'https://checkout.stripe.com/checkout.js',
      :class              => 'stripe-button',
      :'data-name'        => post_display_name(post),
      :'data-description' => stripe_price_description(post),
      :'data-image'       => image_path('site/supost_stripe.jpg'),
      :'data-key'         => Rails.configuration.stripe[:publishable_key],
      :'data-locale'      => 'auto'
    }

    if post.purchasable?
      options.merge!(
        :'data-amount'      => post.purchase_price_in_cents,
        :'data-panel-label' => 'Buy for {{amount}}',
        :'data-label'       => 'Buy'
      )
    else
      options.merge!(
        :'data-amount'      => post.reserve_price_in_cents,
        :'data-panel-label' => 'Reserve for {{amount}}',
        :'data-label'       => "Reserve for #{price_tag(post.reserve_price)}"
      )
    end

    content_tag :script, '', options
  end

  def post_camera_tag(post)
    if post.have_photos?
      content_tag :span, image_tag('site/camera.gif', :class => 'icon_photo'), :class => 'photo-tag'
    else
      ''
    end
  end

  def post_time_tag(post)
    content_tag :span, time_ago_in_words(post.posted_at), :class => 'days-ago'
  end

  def post_body_tag(body)
    if body.present?
      html = simple_format body
      html = auto_link html, :html => { :target => '_blank' }
      html.html_safe
    else
      ''
    end
  end

  def post_photo_tag(path, options = {})
    image = compute_public_path(path, 'images', nil, false)
    tag(:img, options.merge(:src => image))
  end
end
