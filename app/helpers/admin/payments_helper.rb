module Admin::PaymentsHelper
  def stripe_synced_indicator(payment)
    if payment.stripe_synced?
      image_tag 'admin/synced.png'
    end
  end
end
