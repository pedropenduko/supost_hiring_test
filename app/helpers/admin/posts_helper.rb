module Admin::PostsHelper
  def edit_link(post)
    if can_update_post?(post)
      link_to post.truncated_name(40), edit_admin_post_path(post)
    else
      post.truncated_name(40)
    end
  end

  def view_link(post)
    options = {
      :title  => 'View this post',
      :target => '_blank'
    }

    link_to image_tag('admin/info.png'), readonly_admin_post_path(post), options
  end

  def readonly_link(post)
    options = {
      :title     => 'View this post',
      :'data-id' => "post-#{post.id}",
      :class     => 'toggle-post-details',
      :target    => '_blank'
    }
    link_to post.name, readonly_admin_post_path(post), options
  end

  def similar_posts_link(post)
    link_to post.truncated_email, similar_admin_post_path(post)
  end

  def purchase_indicator(post)
    if post.enable_purchases?
      image_tag 'admin/dollar.png'
    end
  end

  def photo_indicator(post)
    if post.have_photos?
      image_tag 'site/camera.gif', :class => 'icon-photo'
    end
  end

  def notify_link(post)
    options = {
      :title   => 'Notify poster',
      :method  => :post,
      :confirm => "Are you sure you want to send publish link of #{post.full_name} to #{post.email.inspect}?"
    }
    link_to image_tag('admin/email.png'), notify_admin_post_path(post), options
  end

  def destroy_link(post)
    options = {
      :title   => 'Delete this post',
      :method  => :delete,
      :confirm => "Are you sure you want to destroy #{post.full_name} permanently?"
    }
    link_to image_tag('admin/delete.png'), admin_post_path(post), options
  end

  def next_action_link(post)
    if post.active? || post.deleted? || post.reserved_or_purchased?
      view_link(post)
    elsif post.confirmed?
      notify_link(post)
    elsif !post.for_off_campus_jobs?
      destroy_link(post)
    end
  end

  def publish_link(post)
    if post.reserved_or_purchased?
      options = {
        :title  => "Publish this post",
        :method => :post
      }

      path = 'admin/publish.png'
      url  = release_admin_post_path(post)
    else
      action = post.inactive? ? 'Publish' : 'Unpublish'

      options = {
        :title  => "#{action} this post",
        :method => :post
      }

      path = post.inactive? ? 'admin/publish.png' : 'admin/unpublish.png'
      url  = publish_admin_post_path(post)
    end

    link_to image_tag(path), url, options
  end

  def report_spam_link(post)
    options = {
      :title   => 'Block this poster',
      :confirm => "Are you sure you want to add #{post.email.inspect} to list of Spammers?",
      :method  => :post,
      :class   => 'action'
    }

    link_to image_tag('admin/add.png'), report_admin_post_path(post), options
  end

  def messages_of_post_link(post)
    options = {
      :title  => 'View messages of this post',
      :target => '_blank',
      :class  => 'action'
    }

    link_to image_tag('admin/messages.png'), admin_post_messages_path(post), options
  end
end
