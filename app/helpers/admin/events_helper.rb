module Admin::EventsHelper
  def view_data_link(event)
    options = {
      :title     => 'View data',
      :'data-id' => event.id,
      :class     => 'view-data'
    }

    link_to image_tag('admin/info.png'), 'javascript:void(0);', options
  end
end
