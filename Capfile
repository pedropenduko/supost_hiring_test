# Load DSL and set up stages
require "capistrano/setup"

# Include default deployment tasks
require "capistrano/deploy"

# Include tasks from other gems included in your Gemfile
#
# For documentation on these, see for example:
#
#   https://github.com/capistrano/rvm
#   https://github.com/capistrano/rbenv
#   https://github.com/capistrano/chruby
#   https://github.com/capistrano/bundler
#   https://github.com/capistrano/rails
#   https://github.com/capistrano/passenger
#
# require 'capistrano/rvm'
require 'capistrano/rbenv'
# require 'capistrano/chruby'
require 'capistrano/bundler'
# require 'capistrano/rails/assets'
require 'capistrano/rails/migrations'
# require 'capistrano/passenger'

# Rollbar Integration
require 'rollbar/capistrano3'

# Load custom tasks from `lib/capistrano/tasks` if you have any defined
Dir.glob("lib/capistrano/tasks/*.rake").each { |r| import r }

def app_name
  fetch(:app_name, 'supost')
end

def nginx_config
  "#{app_name}-puma"
end

def nginx_init
  '/usr/sbin/service nginx'
end

def puma_init
  "/etc/init.d/#{app_name}-puma"
end

def delayed_job_init
  "/etc/init.d/#{app_name}-delayed-job"
end

def rails_env
  fetch(:rails_env)
end

def rbenv_ruby
  fetch(:rbenv_ruby)
end

def wait_for_process_to_end(process_name)
  execute "COUNT=1; until [ $COUNT -eq 0 ]; do COUNT=`ps -ef | grep -v 'ps -ef' | grep -v 'grep' | grep -i '#{process_name}'|wc -l` ; echo 'waiting for #{process_name} to end' ; sleep 2 ; done"
end
