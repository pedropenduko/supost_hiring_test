if Rails.env.production? || Rails.env.staging?
  ActionMailer::Base.perform_deliveries = true
  ActionMailer::Base.raise_delivery_errors = true

  ActionMailer::Base.delivery_method = :smtp
  ActionMailer::Base.smtp_settings = {
    :address              => 'smtp.mailgun.org',
    :port                 => 587,
    :authentication       => :plain,
    :enable_starttls_auto => true,
    :domain               => 'supost.com',
    :user_name            => 'response@mg.supost.com',
    :password             => 'Moonshine946'
  }
end
