# config valid only for current version of Capistrano
lock '3.5.0'

set :rbenv_ruby, '2.0.0-p648'

set :app_name,   'supost'
set :rails_env,  'production'
set :repo_url,   'git@bitbucket.org:suposts/supost.git'

# Default branch is :master
# ask :branch, `git rev-parse --abbrev-ref HEAD`.chomp

# Default deploy_to directory is /var/www/my_app_name
set :deploy_to, "/home/deploy/#{app_name}"

# Default value for :scm is :git
set :scm, :git

# Default value for :format is :airbrussh.
# set :format, :airbrussh

# You can configure the Airbrussh format using :format_options.
# These are the defaults.
# set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: :auto
set :format_options, command_output: true, log_file: 'log/capistrano.log', color: :auto, truncate: false

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
# set :linked_files, fetch(:linked_files, []).push('config/database.yml', 'config/secrets.yml')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('log', 'tmp/pids', 'tmp/cache', 'tmp/sockets', 'public/system')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 7

set :ssh_options, { forward_agent: true }

# Rollbar Settings
set :rollbar_token, '1cf11c8a11504746b845e47845e3bbd3'
set :rollbar_env, Proc.new { fetch :stage }
set :rollbar_role, Proc.new { :app }

namespace :deploy do
  desc 'Start Ruby web server'
  task :start do
    on roles(:app), in: :sequence, wait: 5 do
      execute puma_init, 'start'
    end
  end

  desc 'Stop Ruby web server'
  task :stop do
    on roles(:app), in: :sequence, wait: 5 do
      execute puma_init, 'stop'
    end
  end

  desc 'Restart Ruby web server'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute puma_init, 'restart'
    end
  end

  desc 'Phased-restart web application'
  task :'phased-restart' do
    on roles(:app), in: :sequence, wait: 5 do
      execute puma_init, 'phased-restart'
    end
  end

  desc 'Restart Ruby web server fully'
  task :full_restart do
    on roles(:app), in: :sequence, wait: 5 do
      invoke :'deploy:stop'
      invoke :'deploy:start'
    end
  end

  desc 'Stop current application - remove nginx config and puma init script, then restart nginx'
  task :shutdown do
    on roles(:app), in: :sequence, wait: 5 do
      if test("[ -f #{puma_init} ]")
        execute puma_init, 'stop'
      end
      invoke :'delayed_job:stop'
      invoke :'app:remove'
      invoke :'nginx:restart'
    end
  end

  desc 'Restart current application - copy nginx config and puma init script, then restart nginx'
  task :relaunch do
    on roles(:app), in: :sequence, wait: 5 do
      invoke :'app:setup'
      invoke :'nginx:restart'
      execute puma_init, 'start'
    end
  end

  after :check,      :'app:init'
  after :updating,   :'app:configure'
  after :publishing, :'app:setup'
  after :publishing, :'nginx:restart' unless ENV['FAST']
  after :publishing, :restart
  after :publishing, :'delayed_job:restart'
  after :restart,    :'app:clear_cache'
end

namespace :app do
  desc 'Init folder structure'
  task :init do
    on roles(:app), in: :sequence, wait: 5 do
      execute :mkdir, '-p', File.join(shared_path, 'uploads', 'raw')
      execute :mkdir, '-p', File.join(shared_path, 'uploads', 'post')
      execute :mkdir, '-p', File.join(shared_path, 'uploads', 'ticker')
    end
  end

  desc 'Setup nginx config and init script'
  task :setup do
    on roles(:app), in: :sequence, wait: 5 do
      sudo :cp, '-f', "#{current_path}/config/etc/nginx/sites-available/#{nginx_config} /etc/nginx/sites-enabled/#{nginx_config}"
      sudo :cp, '-f', "#{File.join(current_path, 'config', puma_init)} #{puma_init}"
      sudo :cp, '-f', "#{File.join(current_path, 'config', delayed_job_init)} #{delayed_job_init}"
    end
  end

  desc 'Remove nginx config and init script'
  task :remove do
    on roles(:app), in: :sequence, wait: 5 do
      sudo :rm, '-f', puma_init
      sudo :rm, '-f', delayed_job_init
      sudo :rm, '-f', "/etc/nginx/sites-enabled/#{nginx_config}"
    end
  end

  desc 'Configure app - copy database.yml and mailer settings'
  task :configure do
    on roles(:app), in: :sequence, wait: 5 do
      within release_path do
        execute :cp, '-f', "config/databases/#{rails_env}.yml config/database.yml"
        execute :cp, '-f', 'config/mailer/mailgun.rb config/initializers/mailgun.rb'
        execute :ln, '-nfs', "#{shared_path}/uploads public/uploads"
      end
    end
  end

  desc 'Clear cache'
  task :clear_cache do
    on roles(:web), in: :sequence, wait: 5 do
      within current_path do
        execute :rm, '-f', 'public/index.html'
      end
    end
  end
end

namespace :delayed_job do
  desc 'Start the delayed_job process'
  task :start do
    on roles(:app) do
      execute delayed_job_init, 'start'
    end
  end

  desc 'Stop the delayed_job process'
  task :stop do
    on roles(:app) do
      execute delayed_job_init, 'stop'
    end
  end

  desc 'Force to stop the delayed_job process'
  task :force_stop do
    on roles(:app) do
      execute delayed_job_init, 'force-stop'
    end
  end

  desc 'Restart the delayed_job process'
  task :restart do
    on roles(:app) do
      execute delayed_job_init, 'restart'
    end
  end

  desc 'Restart the delayed_job process fully'
  task :full_restart do
    on roles(:app) do
      execute delayed_job_init, 'full-restart'
    end
  end

  desc 'Run the delayed_job process'
  task :run do
    on roles(:app) do
      execute delayed_job_init, 'run'
    end
  end
end

namespace :nginx do
  desc 'Restart nginx server'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      sudo nginx_init, 'restart'
    end
  end

  desc 'Restart nginx server fully'
  task :full_restart do
    on roles(:app), in: :sequence, wait: 5 do
      sudo nginx_init, 'stop'
      sudo nginx_init, 'start'
    end
  end
end

namespace :maintenance do
  desc 'Enable maintenance page'
  task :on do
    on roles(:web), in: :sequence, wait: 5 do
      within current_path do
        execute :mkdir, '-p', 'public/system'
        execute :cp, '-f', 'public/maintenance.html public/system/maintenance.html'
      end
    end
  end

  desc 'Disable maintenance page'
  task :off do
    on roles(:web), in: :sequence, wait: 5 do
      within current_path do
        execute :mkdir, '-p', 'public/system'
        execute :rm, '-f', 'public/system/maintenance.html'
      end
    end
  end
end

namespace :logs do
  desc 'View rails log'
  task :rails do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/#{rails_env}.log"
    end
  end

  desc "View puma.log"
  task :puma do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/puma.*.log"
    end
  end

  desc 'View both rails and puma log'
  task :all do
    on roles(:app) do
      execute "tail -f #{shared_path}/log/puma.*.log #{shared_path}/log/#{rails_env}.log"
    end
  end

  desc 'Clear logs'
  task :clear do
    on roles(:app) do
      within release_path do
        execute :truncate, '--size 0', "log/#{rails_env}.log"
        execute :truncate, '--size 0', 'log/puma.access.log'
        execute :truncate, '--size 0', 'log/puma.error.log'

        execute :rm, '-f', "log/#{rails_env}.log.*.gz"
        execute :rm, '-f', 'log/puma.*.log.*.gz'
      end
    end
  end
end

namespace :rbenv do
  desc 'Update rbenv and plugins'
  task :update do
    on roles(:app) do
      within '~' do
        execute :rbenv, :update
      end
    end
  end

  desc 'Install a Ruby version, VERSION=<version>'
  task :install do |t, args|
    on roles(:app) do
      version = ENV['VERSION'].to_s.dup.strip

      if version.empty?
        error "Please specify ruby version to install"
        error "Use: rbenv:install VERSION=<version>"
        exit 1
      end

      execute :mkdir, '-p', '~/.rbenv/cache'

      options = []
      if ENV['FORCE']
        options << '--force'
      else
        options << '--skip-existing'
      end
      options << '--verbose' if ENV['VERBOSE']
      options = options.join(' ')

      cmd = "RBENV_ROOT=~/.rbenv RUBY_CONFIGURE_OPTS=--disable-install-doc ~/.rbenv/bin/rbenv install #{options} #{version}"
      execute cmd
    end
  end

  desc "Set Ruby version for RBENV_VERSION=<version>"
  task :set do
    on roles(:app) do
      rbenv = "RBENV_ROOT=~/.rbenv ~/.rbenv/bin/rbenv"

      execute "#{rbenv} local  #{rbenv_ruby}"
      execute "#{rbenv} global #{rbenv_ruby}"

      execute "#{rbenv} local"
      execute "#{rbenv} global"
    end
  end

  namespace :bundler do
    desc 'Install Bundler VERSION=<version> for RBENV_VERSION=<version>'
    task :install do
      on roles(:app) do
        version = ENV['VERSION'].to_s.dup.strip

        options = ['--no-document']
        options << "-v #{version.inspect}" unless version.empty?
        options = options.join(' ')

        cmd = "RBENV_ROOT=~/.rbenv RBENV_VERSION=#{rbenv_ruby} ~/.rbenv/bin/rbenv exec gem install bundler #{options}"
        execute cmd
      end
    end

    desc 'Uninstall all Bundler gems for RBENV_VERSION=<version>'
    task :uninstall do
      on roles(:app) do
        cmd = "RBENV_ROOT=~/.rbenv RBENV_VERSION=#{rbenv_ruby} ~/.rbenv/bin/rbenv exec gem uninstall bundler --all --force"
        execute cmd
      end
    end
  end
end
