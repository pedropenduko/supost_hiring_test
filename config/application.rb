require File.expand_path('../boot', __FILE__)

require 'rails/all'

Bundler.require(:default, Rails.env) if defined?(Bundler)

module Supost
  class Application < Rails::Application
    config.generators do |g|
      g.test_framework nil
      g.view_specs     false
      g.helper_specs   false
    end

    # Custom directories with classes and modules you want to be autoloadable.
    config.autoload_paths += [
      config.root.join('lib'),
      config.root.join('app', 'services')
    ]

    # Configure the default encoding used in templates for Ruby 1.9.
    config.encoding = 'utf-8'

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'
    config.time_zone = 'UTC'

    # Configure sensitive parameters which will be filtered from the log file.
    config.filter_parameters += [:password, :security_id, :access_token, :stripeEmail, :stripeToken, :stripeTokenType]

    # I18n
    config.i18n.enforce_available_locales = false
    config.i18n.default_locale = :en
  end
end

# Fix for Rails 3.0.x under Ruby 2.0+
ActionController::Base.config.relative_url_root = ''
