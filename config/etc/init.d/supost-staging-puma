#!/bin/sh
### BEGIN INIT INFO
# Provides:          supost-staging-puma
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Manage puma server
# Description:       Start, stop, restart puma server for SUpost application.
### END INIT INFO

APP_USER="deploy"
APP_ROOT=/home/$APP_USER/supost-staging/current
RAILS_ENV="staging"
PIDFILE=$APP_ROOT/tmp/pids/puma.pid
STATEFILE=$APP_ROOT/tmp/pids/puma.state
RBENV_ROOT=/home/$APP_USER/.rbenv
RBENV_VERSION="2.0.0-p648"
PATH=$RBENV_ROOT/shims:$RBENV_ROOT/bin:/usr/local/bin:/usr/local/sbin:/sbin:/usr/sbin:/bin:/usr/bin
DESC="SUpost Puma Rack Web Server (staging)"
NAME=supost-staging-puma
SCRIPTNAME=/etc/init.d/$NAME
DAEMON="env RBENV_VERSION=$RBENV_VERSION bundle exec puma"
DAEMON_ARGS="-e $RAILS_ENV -C config/puma/$RAILS_ENV.rb"
CMD="cd $APP_ROOT && $DAEMON $DAEMON_ARGS"
PUMACTL="cd $APP_ROOT && env RBENV_VERSION=$RBENV_VERSION bundle exec pumactl"

do_start_puma() {
    if [ "$(id -un)" = "$APP_USER" ]; then
        eval "$CMD"
    else
        su -c "$CMD" - $APP_USER
    fi
}

do_start() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma isn't running, run it, otherwise restart it.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            do_start_puma
        else
            echo "Puma is running (PID $PID)"
            # do_restart
        fi
    else
        do_start_puma
    fi
    return 0
}

do_stop() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma is running, send QUIT to it, otherwise inform message.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            echo "Puma isn't running"
        else
            kill -s QUIT $PID
            echo "Puma stopped OK"
        fi
        # Cleanup
        rm -f $PIDFILE $STATEFILE
    else
        echo "No puma here"
    fi
    return 0
}

do_force_stop() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma is running, send QUIT to it, otherwise inform message.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            echo "Puma isn't running"
        else
            kill -s TERM $PID
            echo "Puma forced to stop OK"
        fi
        # Cleanup
        rm -f $PIDFILE $STATEFILE
    else
        echo "No puma here"
    fi
    return 0
}

do_status() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma is running, send QUIT to it, otherwise inform message.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            echo "Puma isn't running"
        else
            echo "Puma is running (PID $PID)"
        fi
    else
        echo "No puma here"
    fi
    return 0
}

do_phased_restart() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma isn't running, run it, otherwise restart it.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            echo "Puma isn't running, starting '$CMD' instead"
            do_start_puma
        else
            kill -s USR1 $PID
            echo "Puma restarted (phased) OK"
        fi
    else
        echo "Puma isn't running, starting '$CMD' instead"
        do_start_puma
    fi
    return 0
}

do_restart() {
    if [ -e $PIDFILE ]; then
        PID=`cat $PIDFILE`
        # If the puma isn't running, run it, otherwise restart it.
        if [ "`ps -A -o pid= | grep -c $PID`" -eq 0 ]; then
            echo "Puma isn't running, starting '$CMD' instead"
            do_start_puma
        else
            kill -s USR2 $PID
            echo "Puma restarted OK"
            sleep 5
            do_start
        fi
    else
        echo "Puma isn't running, starting '$CMD' instead"
        do_start_puma
    fi
    return 0
}

case "$1" in
start)
    do_start
    ;;
stop)
    do_stop
    ;;
force-stop)
    do_force_stop
    ;;
status)
    do_status
    ;;
phased-restart)
    do_phased_restart
    ;;
restart)
    do_restart
    ;;
*)
    echo >&2 "Usage: $0 <start|stop|force-stop|status|phased-restart|restart>"
    exit 1
    ;;
esac
