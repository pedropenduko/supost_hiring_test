Supost::Application.routes.draw do
  root :to => 'site#index'

  # Admin
  namespace :admin do
    root :to => 'posts#index'

    resources :events, :only => [:index]

    resources :payments, :only => [:index]

    resources :spammers, :only => [:index, :create, :destroy] do
      member do
        get :posts
      end
    end

    resources :scammers, :only => [:index, :create, :destroy] do
      member do
        get :messages
      end
      collection do
        get :find
      end
    end

    resources :messages, :only => [:index] do
      member do
        get  :similar
        get  :report
        post :report
      end
    end

    get 'profile'  => 'users#profile',  :as => :profile
    put 'profile'  => 'users#profile',  :as => :profile

    get 'password' => 'users#password', :as => :password
    put 'password' => 'users#password', :as => :password

    resources :users do
      member do
        post :reset
      end
    end

    resources :posts do
      member do
        get  :readonly
        post :publish
        post :release
        post :notify
        get  :similar
        get  :report
        post :report
      end
      collection do
        get :job
      end
      resources :messages, :only => :index
    end

    get  'login'  => 'sessions#new',     :as => :login
    post 'login'  => 'sessions#create',  :as => :login
    get  'logout' => 'sessions#destroy', :as => :logout
  end

  mount DelayedJobWeb => '/delayed_job'

  # Stripe
  post '/webhook'         => 'stripe#webhook'
  post '/charge/:post_id' => 'stripe#charge', :as => :charge

  # Site
  get '/about'        => 'site#about',   :as => :about
  get '/contact'      => 'site#contact', :as => :contact
  get '/privacy'      => 'site#privacy', :as => :privacy
  get '/terms'        => 'site#terms',   :as => :terms
  get '/help'         => 'site#help',    :as => :help
  get '/safety'       => 'site#safety',  :as => :safety
  get '/status'       => 'site#state',   :as => :status
  get '/robots.txt'   => 'site#robots',  :as => nil

  # Legacy
  # TODO: Remove this on 2016-10-01
  get '/site(/index)' => 'site#index',   :as => nil
  get '/site/about'   => 'site#about',   :as => nil
  get '/site/contact' => 'site#contact', :as => nil
  get '/site/privacy' => 'site#privacy', :as => nil
  get '/site/terms'   => 'site#terms',   :as => nil
  get '/site/help'    => 'site#help',    :as => nil

  # Add
  get  '/add'                          => 'add#index',   :as => :new_post
  get  '/add/cat/:cat(/sub/:sub)'      => 'add#index',   :as => :add_post
  post '/add/cat/:cat(/sub/:sub)'      => 'add#index',   :as => :add_post
  get  '/add/preview/:id'              => 'add#preview', :as => :preview_post
  post '/add/confirm/:id'              => 'add#confirm', :as => :confirm_post
  get  '/add/success'                  => 'add#success', :as => :success

  # Legacy
  # TODO: Remove this on 2016-10-01
  get  '/add/index(/:cat)' => 'add#index', :as => nil
  post '/add/index/:cat'   => 'add#index', :as => nil

  # Post
  get  '/post/index/:id'             => 'post#index',   :as => :post
  post '/post/index/:id'             => 'post#index',   :as => :post
  get  '/post/publish/:access_token' => 'post#publish', :as => :publish_post
  get  '/post/delete/:access_token'  => 'post#delete',  :as => :delete_post

  # Legacy
  # TODO: Remove this on 2016-10-01
  get '/add/publish/:id/:security_id' => 'post#legacy_publish', :as => nil
  get '/add/publish/:id'              => 'post#legacy_publish', :as => nil
  get '/add/delete/:id/:security_id'  => 'post#legacy_delete',  :as => nil
  get '/add/delete/:id'               => 'post#legacy_delete',  :as => nil

  # Search
  get '/search/cat/:cat'     => 'search#index', :as => :search_by_cat
  get '/search/sub/:sub'     => 'search#index', :as => :search_by_sub
  get '/search'              => 'search#index', :as => :search

  # Legacy
  # TODO: Remove this on 2016-10-01
  get '/search/index(/:cat)' => 'search#index', :as => nil

  # Job
  get '/job'             => 'job#index',       :as => :job
  get '/subscriber'      => 'job#subscriber',  :as => :subscriber
  get '/unsubscribe'     => 'job#unsubscribe', :as => :unsubscribe
  get '/success'         => 'job#success',     :as => nil

  # Legacy
  # TODO: Remove this on 2016-10-01
  get '/job/index'       => 'job#index',       :as => nil
  get '/job/subscriber'  => 'job#subscriber',  :as => nil
  get '/job/unsubscribe' => 'job#unsubscribe', :as => nil

  # Redirect unknown to to home page
  # match '*path' => redirect('/')
end
