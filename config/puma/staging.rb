#!/usr/bin/env puma

# Start Puma with next command:
# RAILS_ENV=staging bundle exec puma -e staging -C ./config/puma/staging.rb

root = File.expand_path('../../../', __FILE__)
rails_env = 'staging'

directory       root
environment     rails_env
daemonize
pidfile         "#{root}/tmp/pids/puma.pid"
state_path      "#{root}/tmp/pids/puma.state"
stdout_redirect "#{root}/log/puma.access.log", "#{root}/log/puma.error.log", true
quiet
threads         1, 5
bind            "unix://#{root}/tmp/sockets/puma.sock"

activate_control_app "unix://#{root}/tmp/sockets/pumactl.sock"

on_restart do
  puts 'Refreshing Gemfile'
  ENV["BUNDLE_GEMFILE"] = "#{root}/Gemfile"
end
