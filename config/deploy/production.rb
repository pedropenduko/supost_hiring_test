# Droplet supost.com on Digital Ocean
server '104.216.116.132', user: 'deploy', roles: %w{app db web}, primary: true

set :branch,    'master'
set :rails_env, 'production'
set :app_name,  'supost'
set :deploy_to, "/home/deploy/#{app_name}"
