# Droplet supost.com on Digital Ocean
server '104.216.116.132', user: 'deploy', roles: %w{app db web}, primary: true

set :branch,    'staging'
set :rails_env, 'staging'
set :app_name,  'supost-staging'
set :deploy_to, "/home/deploy/#{app_name}"
