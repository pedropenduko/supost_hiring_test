path   = Rails.root.join('config', 'stripe.yml')
stripe = YAML.load_file(path)[Rails.env]

Rails.configuration.stripe = stripe.symbolize_keys

Stripe.api_key = Rails.configuration.stripe[:secret_key]
