Delayed::Worker.destroy_failed_jobs = false
Delayed::Worker.sleep_delay         = 5
Delayed::Worker.max_attempts        = 3
Delayed::Worker.max_run_time        = 5.minutes
Delayed::Worker.read_ahead          = 5
Delayed::Worker.default_queue_name  = 'default'
Delayed::Worker.delay_jobs          = !Rails.env.test?
Delayed::Worker.logger            ||= Logger.new(File.join(Rails.root, 'log', 'delayed_job.log'))

if Rails.env.production?
  DelayedJobWeb.use Rack::Auth::Basic do |username, password|
    !!User.authenticate(username, password)
  end
end
