#!/usr/bin/env bash

mkdir -p ~/.tmux/logs


echo '-> Backup /etc/fstab'
cp /etc/fstab /etc/fstab.backup


echo '-> Backup ~/.bashrc'
cp ~/.bashrc ~/.bashrc.backup


echo '-> Disable installing rdoc and ri'
echo 'gem: --no-rdoc --no-ri' > ~/.gemrc


echo '-> Setup ps1 command prompt'
wget --no-check-certificate https://www.dropbox.com/s/c4q2emo6i2eezn8/ps1.sh?dl=1 -O ~/.ps1.sh
echo '[ -f $HOME/.ps1.sh ] && source $HOME/.ps1.sh && ps1_set' >> ~/.bashrc
[ -f $HOME/.ps1.sh ] && source $HOME/.ps1.sh && ps1_set


echo '-> Create /etc/sudoers.d/deploy for deployment'
cat <<CODE > /etc/sudoers.d/deploy

deploy ALL=NOPASSWD: /etc/init.d/nginx
deploy ALL=NOPASSWD: /usr/sbin/service nginx start
deploy ALL=NOPASSWD: /usr/sbin/service nginx stop
deploy ALL=NOPASSWD: /usr/sbin/service nginx restart

deploy ALL=NOPASSWD: /bin/cp -f /home/deploy/supost/current/config/etc/nginx/sites-available/supost-* /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/cp -f /home/deploy/supost/current/config/etc/init.d/supost-*                /etc/init.d/supost-*

deploy ALL=NOPASSWD: /bin/cp -f /home/deploy/supost-staging/current/config/etc/nginx/sites-available/supost-* /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/cp -f /home/deploy/supost-staging/current/config/etc/init.d/supost-*                /etc/init.d/supost-*

deploy ALL=NOPASSWD: /bin/rm -f /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/rm -f /etc/init.d/supost-*

deploy ALL=NOPASSWD: /bin/cp -fv /home/deploy/supost/current/config/etc/nginx/sites-available/supost-* /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/cp -fv /home/deploy/supost/current/config/etc/init.d/supost-*                /etc/init.d/supost-*

deploy ALL=NOPASSWD: /bin/cp -fv /home/deploy/supost-staging/current/config/etc/nginx/sites-available/supost-* /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/cp -fv /home/deploy/supost-staging/current/config/etc/init.d/supost-*                /etc/init.d/supost-*

deploy ALL=NOPASSWD: /bin/rm -fv /etc/nginx/sites-enabled/supost-*
deploy ALL=NOPASSWD: /bin/rm -fv /etc/init.d/supost-*

CODE


echo '-> Create /etc/logrotate.d/deploy'
cat <<CODE > /etc/logrotate.d/deploy
su deploy deploy
/home/deploy/*/shared/log/*.log {
    weekly
    rotate 5
    missingok
    notifempty
    compress
    create 666 deploy deploy
    copytruncate
}
CODE


echo '-> Make swap file /var/swap.img'
swapon -s
touch /var/swap.img
chmod 600 /var/swap.img
dd if=/dev/zero of=/var/swap.img bs=1024k count=1000
mkswap /var/swap.img


echo '-> Enable swap'
echo '/var/swap.img	none	swap	sw	0	0' >> /etc/fstab
swapon /var/swap.img
sysctl -w vm.swappiness=30


echo '-> Install necessary packages and MySQL and Nginx'
apt-get update -qq -y
apt-get install -qq software-properties-common python-software-properties -y
add-apt-repository ppa:nginx/stable -y
apt-get update -qq -y
apt-get dist-upgrade -qq -y
apt-get install -qq \
    htop git-core subversion openssh-server \
    nginx-full geoip-database libgeoip1 \
    vim tmux xsel xclip \
    mysql-server-5.6 mysql-client-5.6 libmysqlclient-dev \
    build-essential autoconf automake libtool bison patch \
    exuberant-ctags libmagickwand-dev libxml2-dev libxslt1-dev openssl libssl-dev \
    libmemcached-dev libreadline-dev libreadline6 libreadline6-dev \
    zlib1g zlib1g-dev libyaml-dev libc6-dev ncurses-term libncurses5-dev \
    libgdbm3 libgdbm-dev libcurl4-openssl-dev libffi-dev libevent1-dev imagemagick python-pip -y

pip install -q gsutil awscli


mv /usr/share/GeoIP/GeoIP.dat /usr/share/GeoIP/GeoIP.dat_bak
cd /usr/share/GeoIP/
wget http://geolite.maxmind.com/download/geoip/database/GeoLiteCountry/GeoIP.dat.gz
gunzip GeoIP.dat.gz
cd


echo '-> Add deploy user'
useradd -G sudo -s /bin/bash -d /home/deploy -U -m deploy


echo '-> Copy ssh dir'
cp -r ~/.ssh /home/deploy/.ssh
chown deploy -R /home/deploy/.ssh


echo '-> Setup rbenv and ps1.sh for deploy user'
su - deploy -c <<CODE /bin/bash
mkdir -p ~/.tmux/logs

echo 'gem: --no-rdoc --no-ri' > ~/.gemrc

echo '.. Install rbenv'
git clone --quiet https://github.com/sstephenson/rbenv.git      ~/.rbenv
git clone --quiet https://github.com/sstephenson/ruby-build.git ~/.rbenv/plugins/ruby-build
git clone --quiet https://github.com/jf/rbenv-gemset.git        ~/.rbenv/plugins/rbenv-gemset
git clone --quiet https://github.com/rkh/rbenv-update.git       ~/.rbenv/plugins/rbenv-update

echo '.. Download ps1.sh'
wget --no-check-certificate https://www.dropbox.com/s/c4q2emo6i2eezn8/ps1.sh?dl=1 -O ~/.ps1.sh

echo '.. Init rbenv and ps1'
cat <<EOS >> ~/.bash_profile
[ -f \\\$HOME/.bash_aliases ] && source \\\$HOME/.bash_aliases
[ -f \\\$HOME/.ps1.sh ] && source \\\$HOME/.ps1.sh && ps1_set

export EDITOR='vim'
export RUBY_CONFIGURE_OPTS='--disable-install-doc'
export PATH=\\\$HOME/.rbenv/bin:\\\$PATH
if which rbenv >/dev/null 2>&1; then
    eval "\\\$(rbenv init -)"
fi
EOS

export RUBY_CONFIGURE_OPTS='--disable-install-doc'
export PATH=\$HOME/.rbenv/bin:\$PATH
eval "\$(rbenv init -)"

mkdir -p ~/.rbenv/cache
mkdir -p ~/.rbenv/sources

echo ".. Install Ruby 1.8.7-p375 and Bundler gem"
rbenv install -k 1.8.7-p375
rbenv rehash
rbenv local 1.8.7-p375
gem install --quiet bundler -v '1.10.6'

echo ".. Install Ruby 2.0.0-p648 and Bundler gem"
rbenv install -k 2.0.0-p648
rbenv rehash
rbenv local 2.0.0-p648
gem install --quiet bundler -v '1.10.6'

rbenv local --unset

CODE


# echo '-> Change password for deploy user'
# passwd deploy


# echo '-> Config /etc/nginx/nginx.conf'
# vim /etc/nginx/nginx.conf


echo '-> Done!'
