namespace :assets do
  desc "Copy assets to S3"
  task :copy => :environment do
    cmd = `which aws 2>/dev/null`.chomp

    if cmd.blank?
      puts "ERROR: aws does not exist"
      exit 1
    end

    env = [
      'env',
      "AWS_CONFIG_FILE=#{File.join(Rails.root, 'config', 'aws', 'config').inspect}",
      "AWS_SHARED_CREDENTIALS_FILE=#{File.join(Rails.root, 'config', 'aws', 'credentials').inspect}"
    ].join(' ')

    local_path = File.join Rails.root, 'public', ''

    if Rails.env.production?
      s3_path = "s3://supost-prod-assets/"
    else
      s3_path = "s3://supost-staging-assets/"
    end

    excludes = '--exclude "*.DS_Store" --exclude "*uploads*" --exclude "*system*"'
    options = "--acl public-read #{excludes}"
    cmd = "#{env} #{cmd} s3 cp #{options} #{local_path} #{s3_path} --recursive"

    puts "=> Running: #{cmd}"
    system(cmd)
  end

  task :cp => :copy

  desc "Sync assets to S3"
  task :sync => :environment do
    cmd = `which aws 2>/dev/null`.chomp

    if cmd.blank?
      puts "ERROR: aws does not exist"
      exit 1
    end

    env = [
      'env',
      "AWS_CONFIG_FILE=#{File.join(Rails.root, 'config', 'aws', 'config').inspect}",
      "AWS_SHARED_CREDENTIALS_FILE=#{File.join(Rails.root, 'config', 'aws', 'credentials').inspect}"
    ].join(' ')

    local_path = File.join Rails.root, 'public', ''

    if Rails.env.production?
      s3_path = "s3://supost-prod-assets/"
    else
      s3_path = "s3://supost-staging-assets/"
    end

    excludes = '--exclude "*.DS_Store" --exclude "*uploads*" --exclude "*system*"'
    options = "--acl public-read #{excludes}"
    options += ' --delete' if ENV['CLEANUP']

    cmd = "#{env} #{cmd} s3 sync #{options} #{local_path} #{s3_path}"

    puts "=> Running: #{cmd}"
    system(cmd)
  end
end
