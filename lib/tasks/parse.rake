namespace :parse do
  desc 'Import all data to parse.com'
  task :import_all => :environment do
    ParseImporter.import_all
  end

  desc 'Import data to parse.com'
  task :import => :environment do
    ParseImporter.import
  end

  desc 'Update post data on parse.com'
  task :update => :environment do
    ParseImporter.update
  end
end
