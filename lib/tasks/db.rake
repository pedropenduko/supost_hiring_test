namespace :db do
  desc "Populate database with sample data"
  task :populate => :environment do
    limit = ENV['LIMIT'].to_i
    limit = 10 if limit == 0
    Post.populate(limit)
  end
end
