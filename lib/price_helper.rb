class PriceHelper
  def self.round(price)
    if price.nil?
      0
    elsif price == price.to_i
      price.to_i
    else
      price.round(2)
    end
  end

  def self.to_cents(amount)
    (amount.round(2) * 100).to_i
  end

  def self.to_dollars(amount)
    (amount / 100.0).round(2)
  end
end
